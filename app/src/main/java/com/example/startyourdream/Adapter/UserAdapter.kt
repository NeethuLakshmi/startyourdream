package com.example.startyourdream.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.startyourdream.Fragment.ProfileFragment
import com.example.startyourdream.Model.User
import com.example.startyourdream.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView


class UserAdapter (private var  mContext:Context,
                    private  var mUser:List<User>,
                    private var isFragment :Boolean= false) :
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    private var firebaseUser: FirebaseUser? = FirebaseAuth.getInstance().currentUser

    class ViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        var username: TextView = itemView.findViewById(R.id.user_name_search)
        var email: TextView = itemView.findViewById(R.id.user_fullname_search)
        var user_profile_image: CircleImageView = itemView.findViewById(R.id.user_profile_image)
        var follow_button: Button = itemView.findViewById(R.id.follow_button_search)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.user_item_layout, parent, false)
        return UserAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mUser.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val user = mUser[position]
        holder.username.text = user.getUsername()
     //   holder.email.text = user.getemail()
        Picasso.get().load(user.getimage()).placeholder(R.drawable.ic_baseline_account)
            .into(holder.user_profile_image)

        checkfollowingUser(user.getUid(), holder.follow_button)

        holder.itemView.setOnClickListener (View.OnClickListener {
            val pref = mContext.getSharedPreferences("PREFS",Context.MODE_PRIVATE).edit()
            pref.putString("profileId",user.getUid())
            pref.apply()
            (mContext as FragmentActivity ).supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_continer,ProfileFragment()).commit()
        })

        holder.follow_button.setOnClickListener {
            if (holder.follow_button.text.toString() == "Follow") {
                firebaseUser?.uid.let { it ->
                    FirebaseDatabase.getInstance().reference.child("Follow").child(it.toString())
                        .child("Following").child(user.getUid())
                        .setValue(true).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                firebaseUser?.uid.let { it ->
                                    FirebaseDatabase.getInstance().reference
                                        .child("Follow").child(user.getUid())
                                        .child("Followers").child(it.toString())
                                        .setValue(true).addOnCompleteListener { task ->
                                            if (task.isSuccessful) {

                                            }
                                        }
                                }
                            }
                        }
                }
            } else {
                firebaseUser?.uid.let { it ->
                    FirebaseDatabase.getInstance().reference.child("Follow")
                        .child(it.toString())
                        .child("Following").child(user.getUid())
                        .removeValue().addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                firebaseUser?.uid.let { it ->
                                    FirebaseDatabase.getInstance().reference
                                        .child("Follow").child(user.getUid())
                                        .child("Followers").child(it.toString())
                                        .removeValue().addOnCompleteListener { task ->
                                            if (task.isSuccessful) {

                                            }
                                        }
                                }

                            }
                        }


                }
            }
        }
    }


    private fun checkfollowingUser(uid: String, followButton: Button) {
       val followingref= firebaseUser?.uid.let { it ->
            FirebaseDatabase.getInstance().reference
                .child("Follow").child(it.toString())
                .child("Following")
        }
        followingref.addValueEventListener(object :ValueEventListener
        {
            override fun onDataChange(datasnapshot: DataSnapshot)
            {
                if (datasnapshot.child(uid).exists())
                {
                followButton.text="Following"
                }
                else{
                    followButton.text="Follow"
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })


    }


}