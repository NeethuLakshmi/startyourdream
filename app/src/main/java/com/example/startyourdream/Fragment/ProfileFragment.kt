package com.example.startyourdream.Fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.startyourdream.AccountSettingActivity
import com.example.startyourdream.Model.User
import com.example.startyourdream.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
class ProfileFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var profileID: String
    private lateinit var firebaseUser: FirebaseUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//
//        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
                // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_profile, container, false)

        firebaseUser= FirebaseAuth.getInstance().currentUser!!

        val pref = context?.getSharedPreferences("PREFS",Context.MODE_PRIVATE)
        if (pref!= null)
        {
            this.profileID= pref.getString("profileId","nono").toString()
        }
        if (profileID== firebaseUser.uid){
            view.edit_profile_button.text="Edit profile"
        }
        else if (profileID!= firebaseUser.uid){
            checkFollowAndFollowingButtonStatus()
        }
getFollowers()
        getFollowings()

        userInfo()
        view. edit_profile_button.setOnClickListener {
       val getButtonText = edit_profile_button.text.toString()
            when{
                getButtonText == "Edit Profile"-> startActivity(Intent(context,AccountSettingActivity::class.java))
        getButtonText == "Follow" ->{

        }
            }

        }
    return view
    }

    private fun checkFollowAndFollowingButtonStatus()
    {
        val followingref= firebaseUser?.uid.let { it ->
            FirebaseDatabase.getInstance().reference
                .child("Follow").child(it.toString())
                .child("Following")
        }
        if (followingref!= null) {
            followingref.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.child(profileID).exists()){
                        view?.edit_profile_button?.text="Following"
                    }
                    else{
                        view?.edit_profile_button?.text="Follow"
                    }
                }


                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            })
        }

    }

    // companion object {
//        /**
//         * Use this factory method to create a new instance of
//         * this fragment using the provided parameters.
//         *
//         * @param param1 Parameter 1.
//         * @param param2 Parameter 2.
//         * @return A new instance of fragment ProfileFragment.
//         */
//        // TODO: Rename and change types and number of parameters
//        @JvmStatic
//        fun newInstance(param1: String, param2: String) =
//            ProfileFragment().apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
//            }
//    }

    private fun getFollowers()
    {
        val followersref= FirebaseDatabase.getInstance().reference
                .child("Follow").child(profileID)
                .child("Followers")

        followersref.addValueEventListener(object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
              if (snapshot.exists())
              {
                  view?.total_Followers?.text = snapshot.childrenCount.toString()
              }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }


    private fun getFollowings()
    {
        val followersref= FirebaseDatabase.getInstance().reference
                .child("Follow").child(profileID)
                .child("Following")

        followersref.addValueEventListener(object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists())
                {
                    view?.total_folloings?.text = snapshot.childrenCount.toString()
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }
    private  fun userInfo()
        {
          val usersRef = FirebaseDatabase.getInstance().getReference().child(
              "Users").child("profileID")
            usersRef.addValueEventListener(object :ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()){
                        val user= snapshot.getValue<User>(User::class.java)
                        Picasso.get().load(user!!.getimage()).placeholder(R.drawable.ic_baseline_account).into(view?.profile_image)
                        view?.profile_fragment_usernane?.text = user.getUsername()
                        view?.bio_profile_frag?.text = user.getbio()
                        //view?.email_profile_frag?.text = user.getemail()
                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }

            })
        }

    override fun onStop() {
        super.onStop()
        val pref = context?.getSharedPreferences("PREFS",Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId",firebaseUser.uid)
        pref?.apply()
    }

    override fun onPause() {
        super.onPause()
        val pref = context?.getSharedPreferences("PREFS",Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId",firebaseUser.uid)
        pref?.apply()
    }

    override fun onDestroy() {
        super.onDestroy()
        val pref = context?.getSharedPreferences("PREFS",Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId",firebaseUser.uid)
        pref?.apply()
    }
}