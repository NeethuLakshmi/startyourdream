package com.example.startyourdream.Model

import android.provider.ContactsContract

class User
{
    private var uid=""
    private var username=""
    private var email= ""
    private var bio= ""
    private var image= ""

    constructor()
constructor(username: String, email: String, bio: String, image: String, uid: String) {
    this.username = username
    this.email = email
    this.bio = bio
    this.image = image
    this.uid = uid
}

    fun getUsername():String {return username}
    fun setUsername(username: String){ this.username= username   }
    fun getUser():String {return email}
    fun setemail(email: String){ this.email= email   }
    fun getbio():String {return bio}
    fun setbio(bio: String){ this.bio= bio   }
    fun getimage():String {return image}
    fun setimage(image: String){ this.image= image   }
    fun getUid():String {return uid}
    fun setUid(uid:String){ this.uid= uid   }


}