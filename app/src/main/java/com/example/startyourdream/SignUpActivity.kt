package com.example.startyourdream

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        haveanaccount.setOnClickListener {
            startActivity(Intent(applicationContext, SignInActivity::class.java)) }
        signupbutton.setOnClickListener {
            val email = RegisEmailEditText.text.toString()
            val password= RegisPasswordEditText.text.toString()
            val confirm_password= RegiconfirmpaswwordEditText.text.toString()
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                RegisEmailEditText.setError("Invalid Email address")
                RegisEmailEditText.isFocusable = true
            }
            else if (password.length < 6 && password == confirm_password)
            {RegisPasswordEditText.setError("Password should contain atleast 6 charecter")
            RegiconfirmpaswwordEditText.setError("Password and Confirm password should same")
            RegiconfirmpaswwordEditText.isFocusable = true
            RegisPasswordEditText.isFocusable = true}
            else{createAccount()}

        }
    }

    private fun createAccount() {
       val username = RegisUNameEditText.text.toString()
       val userid = RegisuseridEditText.text.toString()
        val email = RegisEmailEditText.text.toString()
        val password= RegisPasswordEditText.text.toString()
        val confirm_password= RegiconfirmpaswwordEditText.text.toString()
       val phone_number= RegisPhoneEditText.text.toString()

        when {
            TextUtils.isEmpty(username) -> RegisUNameEditText.isFocusable = true
            TextUtils.isEmpty(userid )-> RegisuseridEditText.isFocusable = true
            TextUtils.isEmpty(email) -> RegisEmailEditText.isFocusable = true
            TextUtils.isEmpty(password) -> RegisPasswordEditText.isFocusable = true
            TextUtils.isEmpty(confirm_password) -> RegisUNameEditText.isFocusable = true
            TextUtils.isEmpty(phone_number) -> RegisPhoneEditText.isFocusable = true
else-> {
    val progressDialog = ProgressDialog(this@SignUpActivity)
    progressDialog.setTitle("sign Up")
    progressDialog.setMessage("please waite ... ")
    progressDialog.setCanceledOnTouchOutside(false)
    progressDialog.show()
    val mAuth = FirebaseAuth.getInstance()
    mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener { task->
        if(task.isSuccessful){
            saveUserInfo(username,userid,email,progressDialog)

        }
        else{
            val message= task.exception.toString()
            Toast.makeText(applicationContext,"Error $message",Toast.LENGTH_LONG).show()
            FirebaseAuth.getInstance().signOut()
             progressDialog.dismiss()
        }
    }
}
    }
}

    private fun saveUserInfo(
        username: String,
        userid: String,
        email: String,
        progressDialog: ProgressDialog
    ) {
        val currentUserId = FirebaseAuth.getInstance().currentUser!!.uid
        val usersRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("Users")
        val userMap= HashMap<String,Any>()
        userMap["uid"]= currentUserId
        userMap["username"]= username.toLowerCase()
        userMap["userid"]= userid.toLowerCase()
        userMap["email"]= email
        userMap["bio"]= "hey i using start your dream "
        userMap["image"]= "gs://start-your-dream-b1bcb.appspot.com/Default Images/default-profile.jpg"

        usersRef.child(currentUserId).setValue(userMap)
            .addOnCompleteListener { task->
                if(task.isSuccessful){
                    progressDialog.dismiss()
                    Toast.makeText(applicationContext,"Your Account Has Been Created",Toast.LENGTH_LONG).show()
                    val intent=  Intent(this@SignUpActivity,MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                }
                else {
                    val message= task.exception.toString()
                    Toast.makeText(applicationContext,"Error $message",Toast.LENGTH_LONG).show()
                    FirebaseAuth.getInstance().signOut()
                    progressDialog.dismiss()
                }
            }

    }


}
