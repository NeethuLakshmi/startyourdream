package com.example.startyourdream

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_account_setting.*
import kotlinx.android.synthetic.main.activity_add_post.*

class AddPostActivity : AppCompatActivity() {
    private var storagepostPicRef: StorageReference?= null
    private var myUrl = ""
    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)

        CropImage.activity()
            .setAspectRatio(2,1)
            .start(this@AddPostActivity)

        storagepostPicRef = FirebaseStorage.getInstance().getReference()
            .child("Posts Pictures")
        save_post_deatile.setOnClickListener { upLoadImage()
        }


    }

    override fun onActivityReenter(resultCode: Int, data: Intent?) {
        super.onActivityReenter(resultCode, data)

        if (resultCode==CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE &&
            resultCode == Activity.RESULT_OK && data!=null ){
            val result= CropImage.getActivityResult(data)
            imageUri= result.uri
            new_post_imageview.setImageURI(imageUri)
        }
        else {
            Toast.makeText(this@AddPostActivity,"Something went wrong on uploading image",Toast.LENGTH_LONG).show()
        }
    }
    private fun upLoadImage() {
when{
    imageUri == null->    Toast.makeText(this@AddPostActivity, "please upload photo ", Toast.LENGTH_LONG).show()
    accont_setting_bio.text.toString() == null ->
        Toast.makeText(this@AddPostActivity, "please write Bio", Toast.LENGTH_LONG).show()

        else ->{
            val progressDialog  = ProgressDialog(this)
            progressDialog.setTitle("adding picture ")
            progressDialog.setMessage("Please wait, we are Updating Profile")
            progressDialog.show()
            val fileref = storagepostPicRef!!.child(System.currentTimeMillis().toString()!!+"jpg")

            var uploadTask: StorageTask<*>
            uploadTask = fileref.putFile(imageUri!!)
            uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful)
                {
                    task.exception?.let { throw it }
                    progressDialog.dismiss()
                }
                return@Continuation fileref.downloadUrl
            }).addOnCompleteListener ( OnCompleteListener<Uri> { task ->
                if (task.isSuccessful){
                    val downloadUrl = task.result
                    myUrl= downloadUrl.toString()
                    val ref = FirebaseDatabase.getInstance().getReference().child("Pots")
                    val postMap= HashMap<String,Any>()
                    val postid =ref.push().key
                    postMap["postid"]= postid!!
                    postMap["discription"]= description_post.text.toString()
                    postMap["publisher"]= FirebaseAuth.getInstance().currentUser.uid
                    postMap["postimage"]= myUrl
                    ref.child(postid).updateChildren(postMap)

                    Toast.makeText(this@AddPostActivity, "Post Updated successfully ", Toast.LENGTH_LONG).show()

                    val intent=  Intent(this@AddPostActivity,MainActivity::class.java)
                    startActivity(intent)
                    finish()
                    progressDialog.dismiss()
                }
                else{
                    progressDialog.dismiss()
                }

            })

        }
}
    }

}