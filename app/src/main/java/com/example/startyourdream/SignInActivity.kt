package com.example.startyourdream

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.activity_sign_up.*


class SignInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        SignUptextView1.setOnClickListener {
            startActivity(Intent(applicationContext, SignUpActivity::class.java))

        }
        loginbutton.setOnClickListener {
            loginUser()
        }
    }

    private fun loginUser() {
        val email= LoginUserNameEditText.text.toString()
        val password = LoginPasswordEditText.text.toString()
        when{
            TextUtils.isEmpty(email) -> LoginUserNameEditText.isFocusable = true
            TextUtils.isEmpty(password) -> LoginPasswordEditText.isFocusable = true
        else -> {
            val progressDialog = ProgressDialog(this@SignInActivity)
            progressDialog.setTitle("Login")
            progressDialog.setMessage("please waite ... ")
            progressDialog.setCanceledOnTouchOutside(false)
            progressDialog.show()
            val mAuth = FirebaseAuth.getInstance()
            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener { task ->
                if (task .isSuccessful){
                    progressDialog.dismiss()
                    val intent=  Intent(this@SignInActivity,MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                }
                else {
                    val message= task.exception.toString()
                    Toast.makeText(applicationContext,"Error $message", Toast.LENGTH_LONG).show()
                    FirebaseAuth.getInstance().signOut()
                    progressDialog.dismiss()
                }
            }
        }
        }
        }



    override fun onStart() {
        super.onStart()
        if (FirebaseAuth.getInstance().currentUser!= null){
            val intent=  Intent(this@SignInActivity,MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }
}